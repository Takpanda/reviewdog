FROM alpine

RUN apk update \
	&& apk add curl git

# install reviewdog
RUN curl -sfL https://raw.githubusercontent.com/reviewdog/reviewdog/master/install.sh | sh -s
RUN reviewdog -version
